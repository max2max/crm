A Django CRM project

Main features:
- Import input data for bills
- Bill processing

## Build Setup for development

``` bash
# install dependencies
pip install -r requirements.txt

# apply migrations
python manage.py migrate

# apply dev fixtures (excluded advertisers.advertiserofferconversion)
python manage.py loaddata apps/bills/fixtures/dev_dump.json

# create super user
python manage.py createsuperuser

# start dev server
python manage.py runserver
```

## Build Setup for production
Tested on:
- Ubuntu 16.04
- Nginx 1.10.3
- Uwsgi 2.0.15
- MySQL 5.7.20
- Python 3.5.2

Configuration files in [/configs/](./configs/) directory.



``` bash
# install dependencies
pip install -r requirements.txt

# apply migrations
python manage.py migrate

# apply fixtures for user groups
python manage.py loaddata apps/advertisers/fixtures/groups.json

# apply fixtures for bill statuses
python manage.py loaddata apps/bills/fixtures/billstatus.json

# create super user for /admin/
python manage.py createsuperuser

# collect static files
python manage.py collectstatic

# make regular imports
bash configs/cron.sh
```

1. Examples for one python ver.>3 interpreter installed. You should use "python3" and "pip3" if installed python ver.2.
2. Checklist for production settings:
- DEBUG = False
- ALLOWED_HOSTS = [$HOSTS]
- Init settings: "from .local import *" - commented. Do not import local settings.

