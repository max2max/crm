from django.core.management.base import BaseCommand
from var_dump import var_dump
import requests
from apps.advertisers.models import Advertiser, AdvertiserUser
from django.conf import settings
from apps.authorization.utils import print_start_time, print_end_time

class Command(BaseCommand):
    help = 'Import Users'
    model = "AdvertiserUser"
    method = "findAll"
    base_url = settings.BASE_URL
    get_all_advertiserusers_url = "%s&Target=%s&Method=%s" % (base_url, model, method)

    def handle(self, *args, **options):
        print_start_time()
        n = 0
        r = requests.get(self.get_all_advertiserusers_url)
        r_json = r.json()
        response_data = r_json.get('response').get('data')
        for item in response_data:
            advertiser_user = response_data[item]['AdvertiserUser']

            del advertiser_user['SHARED_Users2_id']
            del advertiser_user['AFFILIATE_NETWORK_Brands_id']
            del advertiser_user['is_creator']
            del advertiser_user['salt']
            del advertiser_user['user_id']
            del advertiser_user['wants_alerts']
            del advertiser_user['_NETWORK_employees_id']
            advertiser_user['advertiser_id'] = Advertiser.objects.get(unique_id=advertiser_user['advertiser_id'])
            # TODO: 1. Add filter for request 2. Add mapping of fields
            obj, created = AdvertiserUser.objects.update_or_create(
                unique_id=advertiser_user['id'], defaults=advertiser_user)

            if created:
                n+=1

        print("Import done! %s objects created" % n)
        print_end_time()
