from django.core.management.base import BaseCommand
import requests
from apps.advertisers.models import Advertiser, AdvertiserOffer, AdvertiserOfferGoal
from django.conf import settings
from apps.authorization.utils import print_start_time, print_end_time


class Command(BaseCommand):
    help = 'Import Goals'
    model = "Goal"
    method = "findAll"
    base_url = settings.BASE_URL
    get_all_goals_url = "%s&Target=%s&Method=%s&fields[]=default_payout&fields[]=id&fields[]=name&fields[]=status&fields[]=max_payout&fields[]=advertiser_id&fields[]=offer_id" % (base_url, model, method)

    def handle(self, *args, **options):
        print_start_time()
        n = 0
        r = requests.get(self.get_all_goals_url)
        r_json = r.json()
        response_data = r_json.get('response').get('data')

        for item in response_data:
            advertiser_offer_goal = response_data[item]['Goal']
            try:
                data = {
                    'name': advertiser_offer_goal['name'],
                    'advertiser_id': Advertiser.objects.get(unique_id=advertiser_offer_goal['advertiser_id']),
                    'offer_id': AdvertiserOffer.objects.get(unique_id=advertiser_offer_goal['offer_id']),
                    'max_payout': advertiser_offer_goal['max_payout'],
                    'default_payout': advertiser_offer_goal['default_payout'],
                    'status': advertiser_offer_goal['status'],
                }
                obj, created = AdvertiserOfferGoal.objects.update_or_create(unique_id=advertiser_offer_goal['id'], defaults=data)
                if created:
                    n += 1
            except:
                print("Advertiser %s not found" % (advertiser_offer_goal['advertiser_id']))
        print("Import done! %s objects created" % (n))
        print_end_time()
