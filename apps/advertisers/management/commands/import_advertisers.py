from django.core.management.base import BaseCommand
from var_dump import var_dump
import requests
from apps.advertisers.models import Advertiser
from django.conf import settings
from django.contrib.auth.models import User
from apps.authorization.utils import print_start_time, print_end_time


class Command(BaseCommand):
    help = 'Import Advertisers'
    model = "Advertiser"
    method = "findAll"
    base_url = settings.BASE_URL
    get_all_advertisers_url = "%s&Target=%s&Method=%s" % (base_url, model, method)
    # TODO: 1. Add filter for request

    def handle(self, *args, **options):
        print_start_time()
        r = requests.get(self.get_all_advertisers_url)
        r_json = r.json()
        response_data = r_json.get('response').get('data')
        n = 0
        for item in response_data:
            advertiser = response_data[item]['Advertiser']
            try:
                user = User.objects.get(profile__unique_id=advertiser['account_manager_id'])
            except:
                user = None

            data = {
                'account_manager_id': user,
                'address1': advertiser['address1'],
                'address2': advertiser['address2'],
                'city': advertiser['city'],
                'company': advertiser['company'],
                'country': advertiser['country'],
                'unique_id': advertiser['id'],
                'phone': advertiser['phone'],
                'status': advertiser['status'],
            }

            obj, created = Advertiser.objects.update_or_create(
                unique_id=advertiser['id'], defaults=data)

            if created:
                n += 1

        print("Import advertisers done! %s objects created" % n)
        print_end_time()
