from django.core.management.base import BaseCommand
from var_dump import var_dump
import requests
from apps.advertisers.models import Advertiser, AdvertiserOffer, AdvertiserOfferGoal
from django.conf import settings
from apps.authorization.utils import print_start_time, print_end_time


class Command(BaseCommand):
    help = 'Import Offers'
    model = "Offer"
    method = "findAll"
    base_url = settings.BASE_URL
    get_all_offers_url = "%s&Target=%s&Method=%s&fields[]=name&fields[]=max_payout&fields[]=advertiser_id&fields[]=default_payout&fields[]=has_goals_enabled&fields[]=status&fields[]=default_goal_name" % (base_url, model, method)

    def handle(self, *args, **options):
        print_start_time()
        n = 0
        r = requests.get(self.get_all_offers_url)
        r_json = r.json()
        response_data = r_json.get('response').get('data')
        for item in response_data:
            advertiser_offer = response_data[item]['Offer']
            try:
                advertiser_obj = Advertiser.objects.get(unique_id=advertiser_offer['advertiser_id'])
                offer_data = {
                    'name': advertiser_offer['name'],
                    'advertiser_id': advertiser_obj,
                    'max_payout': advertiser_offer['max_payout'],
                    'default_payout': advertiser_offer['default_payout'],
                    'has_goals_enabled': advertiser_offer['has_goals_enabled'],
                    'status': advertiser_offer['status'],
                }
                obj, created = AdvertiserOffer.objects.update_or_create(unique_id=item, defaults=offer_data)
                if created:
                    n += 1
            except:
                print("Advertiser %s not found" % (advertiser_offer['advertiser_id']))
            try:
                default_goal_name = advertiser_offer['default_goal_name'] or 'Принятое действие'
                offer_obj = AdvertiserOffer.objects.get(unique_id=item)
                goal_data = {
                    'name': default_goal_name,
                    'advertiser_id': advertiser_obj,
                    'offer_id': offer_obj,
                    'max_payout': advertiser_offer['max_payout'],
                    'default_payout': advertiser_offer['default_payout'],
                    'status': advertiser_offer['status'],
                }
                unique_id = int(item) + 1000000
                #TODO: make a function which creates a unique_id for default offers goal
                obj, created = AdvertiserOfferGoal.objects.update_or_create(unique_id=unique_id, defaults=goal_data)
            except:
                print("Offer %s not found" % item)
        print("Import offers done! %s objects created" % n)
        print_end_time()
