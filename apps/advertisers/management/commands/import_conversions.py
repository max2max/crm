from django.core.management.base import BaseCommand
from var_dump import var_dump
import requests
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from apps.advertisers.models import Advertiser, AdvertiserOffer, AdvertiserOfferGoal, AdvertiserOfferConversion
from django.conf import settings
from apps.authorization.utils import print_start_time, print_end_time
import datetime


class Command(BaseCommand):
    help = 'Import Conversions'
    model = "Conversion"
    method = "findAll"
    base_url = settings.BASE_URL

    def handle(self, *args, **options):
        print_start_time()

        current_date = datetime.date.today()
        half_of_month = datetime.timedelta(days=16)
        start_date = current_date - half_of_month

        current_date_iso = current_date.isoformat()
        start_date_iso = start_date.isoformat()

        date_from = start_date_iso
        date_to = current_date_iso

        print("Importing conversions from %s to %s" % (date_from, date_to))

        advertisers = Advertiser.objects.filter(status='active')
        for advertiser in advertisers:
            advertiser_id = advertiser.unique_id
            print("Current advertiser is %s" % advertiser)

            limit = 100
            page_count = 1
            page = 1
            while page <= page_count:
                get_all_conversions_url = "%s&Target=%s&Method=%s&fields[]=status&fields[]=datetime&fields[]=status_code&fields[]=revenue&fields[]=goal_id&fields[]=id&fields[]=offer_id&fields[]=advertiser_id&filters[status]=approved&filters[datetime][GREATER_THAN_OR_EQUAL_TO]=%s&filters[datetime][LESS_THAN_OR_EQUAL_TO]=%s&filters[advertiser_id]=%s&limit=%s&page=%s" % (
                    self.base_url, self.model, self.method, date_from, date_to, advertiser_id, limit, page)
                conversion_count = 0
                http_code = 0
                n = 0
                while n < 5:
                    r = requests.get(get_all_conversions_url)
                    n += 1
                    http_code = r.status_code

                    if http_code != 200:
                        print("Http code : %s, attempt number: %s. Trying again ...\r\nRequest URL: %s" % (http_code, n, get_all_conversions_url))
                    else:
                        print("Http code : %s, successful attempt number: %s." % (http_code, n))
                        break
                try:
                    r_json = r.json()
                except:
                    print("Response is not in json format.")
                    break
                response_data = r_json.get('response').get('data')
                try:
                    page_count = int(response_data['pageCount'])
                    print("Current page is %s of %s" % (page, page_count))
                    page += 1
                except:
                    print("Incorrect page_count value: %s" % page_count)
                    break

                try:
                    conversion_data = response_data.get('data')
                except:
                    var_dump(response_data)
                    break


                for item in conversion_data:
                    advertiser_offer_conversion = conversion_data[item]['Conversion']
                    goal_imported_id = int(advertiser_offer_conversion['goal_id'])
                    goal_id = goal_imported_id if goal_imported_id != 0 else int(advertiser_offer_conversion['offer_id']) + 1000000

                    naive = parse_datetime(advertiser_offer_conversion['datetime'])
                    date_time = timezone.make_aware(naive, timezone.get_current_timezone())

                    try:
                        offer = AdvertiserOffer.objects.get(unique_id=advertiser_offer_conversion['offer_id'])
                    except:
                        print("Offer %s is not found." % advertiser_offer_conversion['offer_id'])
                        offer = None

                    try:
                        goal = AdvertiserOfferGoal.objects.get(unique_id=goal_id)
                    except:
                        print("Goal %s is not found." % advertiser_offer_conversion['goal_id'])
                        goal = None

                    data = {
                        'date_time': date_time,
                        'advertiser_id': Advertiser.objects.get(unique_id=advertiser_offer_conversion['advertiser_id']),
                        'offer_id': offer,
                        'goal_id': goal,
                        'revenue': advertiser_offer_conversion['revenue'],
                        'status': advertiser_offer_conversion['status'],
                        'status_code': advertiser_offer_conversion['status_code'],
                        'created_date_time': timezone.now(),
                    }

                    obj, created = AdvertiserOfferConversion.objects.update_or_create(unique_id=item, defaults=data)
                    if created:
                        conversion_count += 1

            print("Import conversions for advertiser %s is done. Created %s conversions." % (advertiser_id, conversion_count))
        print_end_time()
