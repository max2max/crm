# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-03 13:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('advertisers', '0021_advertiseroffergoal_status'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='advertiseroffer',
            options={'ordering': ['status', '-unique_id']},
        ),
        migrations.AlterModelOptions(
            name='advertiseroffergoal',
            options={'ordering': ['-unique_id']},
        ),
        migrations.AddField(
            model_name='advertiser',
            name='billing_period',
            field=models.PositiveSmallIntegerField(default=2, help_text='1 or 2 times a month bills are created'),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='vat_enabled',
            field=models.BooleanField(default=False, help_text='Enabled VAT? (1/0)'),
        ),
    ]
