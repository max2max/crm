# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-27 15:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('advertisers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdvertiserUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('access', models.TextField(help_text='An array of permissions that the Advertiser User has')),
                ('cell_phone', models.TextField(blank=True, help_text='The Advertiser User`s cell phone', null=True)),
                ('email', models.TextField(help_text='The Advertiser User`s email address')),
                ('first_name', models.TextField(blank=True, help_text='The Advertiser User`s first email', null=True)),
                ('join_date', models.DateTimeField(help_text='The date the Advertiser User was created')),
                ('last_login', models.DateTimeField(blank=True, help_text='The last time the Advertiser User logged into the application', null=True)),
                ('last_name', models.TextField(blank=True, help_text='The Advertiser User`s last name', null=True)),
                ('modified', models.DateTimeField(help_text='The last time the Advertiser User account was updated')),
                ('password', models.TextField(help_text='The Advertiser User`s password')),
                ('password_confirmation', models.TextField(help_text='Provided on creation and must match the provided password field')),
                ('permissions', models.IntegerField(help_text='The Advertiser User`s permissions stored as a bitmask')),
                ('phone', models.TextField(blank=True, help_text='The Advertiser User`s phone number', null=True)),
                ('title', models.TextField(blank=True, help_text='The Advertiser User`s title', null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='advertiser',
            name='title',
        ),
        migrations.AddField(
            model_name='advertiser',
            name='account_manager_id',
            field=models.PositiveIntegerField(blank=True, help_text='ID of Employee object of Account Manager for this Advertiser', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='address1',
            field=models.TextField(blank=True, help_text='The first line of the Advertiser`s physical address', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='address2',
            field=models.TextField(blank=True, help_text='The second line of the Advertiser`s physical address', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='city',
            field=models.TextField(blank=True, help_text='The city of the Advertiser`s physical address', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='company',
            field=models.TextField(blank=True, help_text='The company name of the Advertiser', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='conversion_security_token',
            field=models.TextField(blank=True, help_text='The Advertiser Security Token that must be passed by the advertiser in order to register a conversion.', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='country',
            field=models.TextField(blank=True, help_text='The country of the Advertiser`s physical address. ISO 3166-1 alpha-2 country code', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='fax',
            field=models.TextField(blank=True, help_text='The fax number of the Advertiser', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='phone',
            field=models.TextField(blank=True, help_text='The phone number of the Advertiser', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='ref_id',
            field=models.TextField(blank=True, help_text='An external reference ID for this account', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='region',
            field=models.TextField(blank=True, help_text='The state/provice of the Advertiser`s physical address', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='signup_ip',
            field=models.TextField(blank=True, help_text='The IP address that the Advertiser used to sign up with', null=True),
        ),
        migrations.AddField(
            model_name='advertiseruser',
            name='advertiser',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='advertiser_users', to='advertisers.Advertiser'),
        ),
    ]
