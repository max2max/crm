# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-30 12:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('advertisers', '0007_advertiseroffer'),
    ]

    operations = [
        migrations.AddField(
            model_name='advertiseroffer',
            name='max_payout',
            field=models.DecimalField(blank=True, decimal_places=2, help_text='Payout for offer', max_digits=6, null=True),
        ),
        migrations.AddField(
            model_name='advertiseroffer',
            name='name',
            field=models.TextField(blank=True, help_text='Name of the offer', null=True),
        ),
        migrations.AddField(
            model_name='advertiseroffer',
            name='unique_id',
            field=models.IntegerField(blank=True, help_text='External unique ID for this AdvertiserOffer', null=True, unique=True),
        ),
    ]
