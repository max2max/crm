from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Advertiser(models.Model):
    """
        An Advertiser account
    """

    begin_billing_period_date = models.DateField(
        blank=True,
        null=True,
    )
    end_billing_period_date = models.DateField(
        blank=True,
        null=True,
    )

    billing_period = models.PositiveSmallIntegerField(
        blank=False,
        null=False,
        default=2,
        help_text='1 or 2 times a month bills are created'
    )

    vat_enabled = models.BooleanField(
        default=False,
        blank=True,
        help_text='Enabled VAT? (1/0)'
    )

    # Tune dev hub specific fields
    # https://developers.tune.com/network-models/advertiser/

    account_manager_id = models.ForeignKey(
        User,
        blank=True,
        null=True,
        help_text='ID of User Object of Account Manager for this Advertiser',
    )

    address1 = models.TextField(
        help_text='The first line of the Advertiser`s physical address',
        blank=True,
        null=True,
    )

    address2 = models.TextField(
        blank=True,
        null=True,
        help_text='The second line of the Advertiser`s physical address',
    )

    city = models.TextField(
        blank=True,
        null=True,
        help_text='The city of the Advertiser`s physical address',
    )

    company = models.TextField(
        blank=True,
        null=True,
        help_text='The company name of the Advertiser',
    )

    country = models.TextField(
        blank=True,
        null=True,
        help_text='The country of the Advertiser`s physical address. ISO 3166-1 alpha-2 country code',
    )

    unique_id = models.IntegerField(
        unique=True,
        blank=True,
        null=True,
        help_text='ID of unique, auto-generated object for this Advertiser',
    )

    phone = models.TextField(
        blank=True,
        null=True,
        help_text='The phone number of the Advertiser'
    )

    status = models.TextField(
        help_text='The status of the Advertiser account',
        blank=True,
        null=True,
    )

    def __str__(self):
        return "%s %s" % (self.unique_id, self.company)

    class Meta:
        ordering = ['unique_id']


class AdvertiserUser(models.Model):
    """
        A user belonging to an Advertiser account.
    """

    # Tune dev hub specific fields
    # https://developers.tune.com/network-models/advertiseruser/

    access = models.TextField(
        help_text='An array of permissions that the Advertiser User has',
    )

    advertiser_id = models.ForeignKey('Advertiser', related_name='advertiser_users')

    cell_phone = models.TextField(
        blank=True,
        null=True,
        help_text='The Advertiser User`s cell phone',
    )

    email = models.TextField(
        help_text='The Advertiser User`s email address',
    )

    first_name = models.TextField(
        blank=True,
        null=True,
        help_text='The Advertiser User`s first email',
    )

    join_date = models.DateTimeField(
        help_text='The date the Advertiser User was created',
    )

    last_login = models.DateTimeField(
        blank=True,
        null=True,
        help_text='The last time the Advertiser User logged into the application',
    )

    last_name = models.TextField(
        blank=True,
        null=True,
        help_text='The Advertiser User`s last name',
    )

    modified = models.DateTimeField(
        help_text='The last time the Advertiser User account was updated',
    )

    password = models.TextField(
        help_text='The Advertiser User`s password',
    )

    password_confirmation = models.TextField(
        help_text='Provided on creation and must match the provided password field',
    )

    permissions = models.IntegerField(
        help_text='The Advertiser User`s permissions stored as a bitmask',
    )

    phone = models.TextField(
        blank=True,
        null=True,
        help_text='The Advertiser User`s phone number',
    )

    status = models.TextField(
        help_text='The Advertiser User status',
        blank=True,
        null=True,
    )

    title = models.TextField(
        blank=True,
        null=True,
        help_text='The Advertiser User`s title',
    )

    unique_id = models.IntegerField(
        unique=True,
        blank=True,
        null=True,
        help_text='External unique ID for this AdvertiserUser',
    )

    def __str__(self):
        return "%s %s - %s - %s" % (self.first_name, self.last_name, self.email, self.advertiser_id)


class AdvertiserOffer(models.Model):
    """
        An offer belonging to an Advertiser account.
        https://developers.tune.com/network-models/offer/
    """

    unique_id = models.IntegerField(
        unique=True,
        blank=True,
        null=True,
        help_text='External unique ID for this AdvertiserOffer',
    )

    name = models.TextField(
        blank=True,
        null=True,
        help_text='Name of the offer',
    )

    max_payout = models.DecimalField(
        blank=True,
        null=True,
        decimal_places=2,
        max_digits=6,
        help_text='Max payout for offer',
    )

    default_payout = models.DecimalField(
        blank=True,
        null=True,
        decimal_places=2,
        max_digits=6,
        help_text='Default payout for offer',
    )

    status = models.TextField(
        blank=True,
        null=True,
        help_text='Offer status',
    )

    has_goals_enabled = models.BooleanField(
        blank=True,
        default=False,
        help_text='Has an offer goals or don`t have (1/0)',
    )

    advertiser_id = models.ForeignKey('Advertiser', related_name='advertiser_offers')

    class Meta:
        ordering = ['status','-unique_id']

    def __str__(self):
        return "%s %s - %s" % (self.unique_id,self.name,self.advertiser_id)


class AdvertiserOfferGoal(models.Model):
    """
        A Goal that belongs to Advertiser and Offer
        https://developers.tune.com/network-models/offer/
    """
    unique_id = models.IntegerField(
        unique=True,
        blank=True,
        null=True,
        help_text='External unique ID for this AdvertiserOfferGoal',
    )

    name = models.TextField(
        blank=True,
        null=True,
        help_text='Name of the goal',
    )

    advertiser_id = models.ForeignKey('Advertiser')

    offer_id = models.ForeignKey('AdvertiserOffer')

    max_payout = models.DecimalField(
        blank=True,
        null=True,
        decimal_places=2,
        max_digits=6,
        help_text='Max payout for Goal',
    )

    default_payout = models.DecimalField(
        blank=True,
        null=True,
        decimal_places=2,
        max_digits=6,
        help_text='Default payout for Goal',
    )

    status = models.TextField(
        blank=True,
        null=True,
        help_text='Goal status',
    )

    def __str__(self):
        return "%s - %s" % (self.unique_id, self.name)

    class Meta:
        ordering = ['-unique_id']


class AdvertiserOfferConversion(models.Model):
    """
        A Goal that belongs to Advertiser and ( Offer or Goal )
        https://developers.tune.com/network-models/conversion/
    """
    unique_id = models.IntegerField(
        unique=True,
        null=True,
        blank=True,
        help_text='External unique ID for conversion',
    )

    date_time = models.DateTimeField(
        null=True,
        blank=True,
        help_text="Time of conversion event",
    )

    advertiser_id = models.ForeignKey(
        'Advertiser',
        null=True,
    )

    offer_id = models.ForeignKey(
        'AdvertiserOffer',
        null=True,
    )

    goal_id = models.ForeignKey(
        'AdvertiserOfferGoal',
        null=True,
        blank=True,
    )

    revenue = models.FloatField(
        blank=True,
        null=True,
        help_text="Revenue of conversion",
    )

    status = models.TextField(
        blank=True,
        null=True,
        help_text="Text status of conversion: approved/rejected/pending",
    )

    status_code = models.IntegerField(
        blank=True,
        null=True,
        help_text="Status code of conversion. See definition on http://support.hasoffers.com/hc/en-us/articles/203507933-Conversion-Status-Codes",
    )

    created_date_time = models.DateTimeField(
        null=True,
        blank=True,
        help_text="Time of import this entry",
    )

    def __str__(self):
        return "%s - %s" % (self.unique_id, self.offer_id)


class Profile(models.Model):
    """
        Model profile extends default User Model (1-1)
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')

    unique_id = models.IntegerField(
        unique=True,
        null=True,
        blank=True,
        help_text='External unique ID for Employee',
    )

    domain = models.TextField(
        default="1"
    )

    """
        Hooks for create and update profile with User Model    
    """

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()