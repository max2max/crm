from django.http import HttpResponse
from django.views import View
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.conf import settings
from apps.advertisers.models import Advertiser, AdvertiserOffer, AdvertiserOfferConversion
from apps.bills.models import Bill
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user
from var_dump import var_dump
from datetime import datetime, timedelta


class AdvertiserList(View):
    @method_decorator(login_required)
    def get(self, request):
        user = get_user(request)
        filter_for_query = dict()
        if user.is_superuser or user.groups.filter(name='accountant').exists() or user.groups.filter(name='statistics').exists():
            filter_for_query = dict()
        elif user.groups.filter(name='manager').exists():
            filter_for_query = {'account_manager_id': user.pk}

        advertisers = Advertiser.objects.filter(**filter_for_query).order_by('status', 'unique_id')
        for advertiser in advertisers:
            last_bill = Bill.objects.filter(advertiser_id=advertiser).order_by('pk').last()
            advertiser.bill = last_bill

        context = {
            'advertisers': advertisers,
        }
        template = loader.get_template('advertisers/advertiser_list.html')
        return HttpResponse(template.render(context, request))


class AdvertiserDetail(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        message = kwargs['message'] if 'message' in dict.keys(kwargs) else ''
        unique_id = kwargs['unique_id']
        template = loader.get_template('advertisers/advertiser_detail.html')
        advertiser = get_object_or_404(Advertiser, unique_id=unique_id)
        offers = AdvertiserOffer.objects.filter(advertiser_id=advertiser)
        offers_base_url = settings.BASE_ACC_OFFER_URL
        account = advertiser.account_manager_id
        bills = Bill.objects.filter(advertiser_id=advertiser).order_by('-pk')

        graph_data = dict()
        dates = AdvertiserOfferConversion.objects.filter(advertiser_id=advertiser).dates('date_time', 'day').distinct()

        for offer in offers:
            graph_data[offer] = dict()
            for date in dates:
                conversions = AdvertiserOfferConversion.objects.filter(advertiser_id=advertiser, offer_id=offer, date_time__range=(date, date+timedelta(days=1))).count()
                graph_data[offer][date] = conversions
        context = {
            'advertiser': advertiser,
            'user': request.user,
            'offers': offers,
            'offers_base_url': offers_base_url,
            'message': message,
            'account': account,
            'bills': bills,
            'graph_dates': dates,
            'graph_data': graph_data,
        }

        return HttpResponse(template.render(context, request))

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        unique_id = kwargs['unique_id']

        vat_enabled = request.POST['vat_enabled']
        billing_period = request.POST['billing_period']

        advertiser = Advertiser.objects.get(unique_id=unique_id)

        advertiser.vat_enabled = vat_enabled
        advertiser.billing_period = billing_period
        try:
            advertiser.save()
            message = 'Настройки рекламодателя успешно обновлены.'
        except:
            message = 'Ошибка при сохранении данных рекламодателя.'
        kwargs['message'] = message
        return self.get(request, *args, **kwargs)
