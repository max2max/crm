from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Advertiser, AdvertiserUser, AdvertiserOffer, AdvertiserOfferGoal, AdvertiserOfferConversion, Profile


@admin.register(AdvertiserUser, AdvertiserOfferGoal)
class AdvertiserAdmin(admin.ModelAdmin):
    pass


# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'


# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (ProfileInline, )


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)


class GoalInline(admin.StackedInline):
    model = AdvertiserOfferGoal
    extra = 0


class AdvertiserOfferAdmin(admin.ModelAdmin):
    inlines = [GoalInline]
    list_display = ('unique_id', '__str__', 'max_payout', 'has_goals_enabled', 'status')
    list_display_links = ('unique_id', '__str__')
    search_fields = ['unique_id', 'name']


admin.site.register(AdvertiserOffer, AdvertiserOfferAdmin)


class AdvertiserUserInline(admin.StackedInline):
    model = AdvertiserUser
    extra = 0


class AdvertiserAdmin(admin.ModelAdmin):
    inlines = [AdvertiserUserInline]
    list_display = ('unique_id', '__str__', 'account_manager_id', 'billing_period', 'status',)
    list_display_links = ('unique_id', '__str__')
    search_fields = ['unique_id']
    list_editable = ('billing_period',)
    list_per_page = 500


admin.site.register(Advertiser, AdvertiserAdmin)


class AdvertiserOfferConversionAdmin(admin.ModelAdmin):
    list_display = ('unique_id', '__str__', 'revenue', 'date_time', 'created_date_time', 'status')


admin.site.register(AdvertiserOfferConversion, AdvertiserOfferConversionAdmin)
