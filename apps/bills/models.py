from django.db import models
from apps.advertisers.models import Advertiser, AdvertiserOffer, AdvertiserOfferGoal, AdvertiserOfferConversion
from django.contrib.auth.models import Group, User


class BillStatus(models.Model):
    TYPES = (
        ('stat', 'statistics'),
        ('acc', 'accountant'),
        ('all', 'all types'),
    )

    name = models.TextField(
        blank=True,
        help_text="Name of status",
    )

    ordering = models.PositiveIntegerField(
        default=555,
        help_text="Bill ordering for workflow,! 0 is always 1 status!",
    )

    action_forward_name = models.TextField(
        blank=True,
        default='Подтвердить',
        help_text='Name of action for next state',
    )

    action_backward_name = models.TextField(
        blank=True,
        default='Вернуть',
        help_text='Name of action for previous state',
    )

    status_forward_id = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='status_forward',
    )

    status_backward_id = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='status_backward',
    )

    change_permitted = models.ForeignKey(
        Group,
    )

    type = models.CharField(
        max_length=4,
        choices=TYPES,
        help_text='workflow'
    )

    is_last = models.BooleanField(
        blank=True,
        default=False,
        help_text="Is the last status for this type of workflow?",
    )

    what_is_need_to_do = models.TextField(
        blank=True,
        help_text="What needs to do a user with bill in this status. Displays on blocks on main page."
    )

    class Meta:
        ordering = ['ordering']

    def __str__(self):
        return "%s - %s - %s" % (self.ordering, self.type, self.name)


class Bill(models.Model):
    code = models.TextField(
        blank=True,
        null=True,
        help_text='Code of bill in 1C',
    )

    create_date = models.DateField(
        blank=True,
        null=True,
        help_text='The date bill was created in',
    )

    create_time = models.TimeField(
        blank=True,
        null=True,
        help_text='The time bill was created in',
    )

    period_from = models.DateField(
        blank=True,
        null=True,
        help_text='Start of billing period',
    )

    period_to = models.DateField(
        blank=True,
        null=True,
        help_text='End of billing period'
    )

    advertiser_id = models.ForeignKey(Advertiser, related_name='advertiser')

    vat_enabled = models.BooleanField(
        blank=True,
        default=False,
        help_text='Enabled VAT for this Bill',
    )

    amount_without_vat = models.FloatField(
        blank=True,
        null=True,
        help_text="Total bill amount excluded VAT",
    )

    vat_amount = models.FloatField(
        blank=True,
        null=True,
        help_text="Total bill VAT amount",
    )

    amount_with_vat = models.FloatField(
        blank=True,
        null=True,
        help_text="Total bill amount included VAT - *key* amount",
    )

    confirmed = models.BooleanField(
        blank=True,
        default=False,
        help_text="Is bill confirmed by the manager",
    )

    time_last_confirmed = models.DateTimeField(
        blank=True,
        null=True,
        help_text="Time when the bill was confirmed last time",
        auto_now=True,
    )

    is_stats_upload_necessary = models.BooleanField(
        blank=True,
        default=True,
        help_text="Enables stats worflow",
    )

    acc_status = models.ForeignKey(
        BillStatus,
        limit_choices_to={'type': 'acc'},
        null=True,
        blank=True,
        related_name='acc_status',
        default=1,
    )

    stat_status = models.ForeignKey(
        BillStatus,
        limit_choices_to={'type': 'stat'},
        null=True,
        blank=True,
        related_name='stat_status',
        default=7,
    )

    def __str__(self):
        return "Счет номер %s от %s на сумму %s" % (self.code, self.create_date, self.amount_with_vat)

    @property
    def is_sum_added(self):
        result = ''
        if self.confirmed and self.amount_with_vat:
            result = {
                'date': self.time_last_confirmed,
                'amount': self.amount_with_vat,
            }
        return result or None

    @property
    def is_stat_sent(self):
        result = ''
        if self.stat_status and self.stat_status.ordering >= 10:
            datetime = BillStatusHistory.objects.filter(bill_id=self, status_id__ordering=0, status_id__type='stat').order_by('-pk').select_related('status_id').first()
            if datetime:
                result = datetime.date_time
        return result or None

    @property
    def is_stat_uploaded(self):
        result = ''
        if self.stat_status and self.stat_status.ordering >= 20:
            datetime = BillStatusHistory.objects.filter(bill_id=self, status_id__ordering=10, status_id__type='stat').order_by('-pk').select_related('status_id').first()
            if datetime:
                result = datetime.date_time
        return result or None

    @property
    def is_bill_sent(self):
        result = ''
        if self.acc_status and self.acc_status.ordering >= 20:
            datetime = BillStatusHistory.objects.filter(bill_id=self, status_id__ordering=10, status_id__type='acc').order_by('-pk').select_related('status_id').first()
            if datetime:
                result = datetime.date_time
        return result or None

    @property
    def is_stat_checked(self):
        result = ''
        if self.stat_status and self.stat_status.ordering >= 30:
            datetime = BillStatusHistory.objects.filter(bill_id=self, status_id__ordering=20, status_id__type='stat').order_by('-pk').select_related('status_id').first()
            if datetime:
                result = datetime.date_time
        return result or None

    @property
    def all_is_ready(self):
        result = ''
        if self.is_sum_added and self.is_bill_sent and self.is_stat_sent and self.is_stat_uploaded and self.is_stat_checked:
            result = True
        return result or None

    # TODO: add role checking
    @property
    def possible_actions(self):
        result = []
        if self.acc_status and self.acc_status.is_last is False:
            result.append(self.acc_status.action_forward_name)
        if self.stat_status and self.stat_status.is_last is False:
            result.append(self.stat_status.action_forward_name)
        return result or None


class BillEntry(models.Model):
    bill_id = models.ForeignKey(
        Bill,
        on_delete=models.CASCADE,
    )

    offer_id = models.ForeignKey(
        AdvertiserOffer,
        blank=True,
        null=True,
    )

    goal_id = models.ForeignKey(
        AdvertiserOfferGoal,
        blank=True,
        null=True,
    )

    name = models.TextField(
        blank=True,
        null=True,
        help_text="Name of bill entry: Offer name from HasOffers or custom name filled in by manager",
    )

    leads = models.PositiveIntegerField(
        blank=True,
        null=True,
        help_text="Amount of leads, imported from HasOffers or edited/filled in manually by manager",
    )

    cost = models.FloatField(
        blank=True,
        null=True,
        help_text="Cost of the one lead, imported from HasOffers or edited/filled in manually by manager",
    )

    amount_without_vat = models.FloatField(
        blank=True,
        null=True,
        help_text="Total entry amount excluded VAT",
    )

    vat_amount = models.FloatField(
        blank=True,
        null=True,
        help_text="Total entry VAT amount",
    )

    amount_with_vat = models.FloatField(
        blank=True,
        null=True,
        help_text="Total entry amount",
    )

    conversions = models.ManyToManyField(
        AdvertiserOfferConversion,
        blank=True,
    )

    def __str__(self):
        return "%s - %s" % (self.name, self.offer_id)


class BillComments(models.Model):
    TYPES = (
        ('stat', 'statistics'),
        ('acc', 'accountant'),
        ('all', 'all types'),
    )

    type = models.CharField(
        max_length=4,
        choices=TYPES,
        help_text='workflow'
    )

    text = models.TextField(
        null=True,
        blank=True,
        help_text="Text of comment",
    )

    create_date = models.DateTimeField(
        null=True,
        blank=True,
        help_text="Date of comment",
        auto_now_add=True,
    )

    bill_id = models.ForeignKey(
        Bill,
        on_delete=models.CASCADE,
        help_text='Bill that comment relates to',
    )

    user_id = models.ForeignKey(
        User,
        null=True,
        blank=True,
        help_text='User posted a comment',
    )


def user_directory_path(instance, filename):
    return "uploads/%s/%s/%s" % (instance.bill_id.pk, instance.type, filename)


class BillFiles(models.Model):
    TYPES = (
        ('stat', 'statistics'),
        ('acc', 'accountant'),
        ('all', 'all types'),
    )

    bill_id = models.ForeignKey(
        Bill,
        null=True,
        on_delete=models.CASCADE,
    )

    entry_id = models.ForeignKey(
        BillEntry,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    file = models.FileField(
        blank=True,
        upload_to=user_directory_path,
    )

    create_date = models.DateTimeField(
        null=True,
        blank=True,
        help_text="Date of fileupload",
        auto_now_add=True,
    )

    type = models.CharField(
        max_length=4,
        choices=TYPES,
        help_text='Type of workflow/personal, who can add comment'
    )


class BillStatusHistory(models.Model):
    date_time = models.DateTimeField(
        null=True,
        blank=True,
        help_text="Datetime of changing status",
        auto_now_add=True,
    )

    user_id = models.ForeignKey(
        User,
        null=True,
        blank=True,
        help_text="User who made changes",
    )

    bill_id = models.ForeignKey(
        Bill,
        null=True,
        blank=True,
        help_text="Bill that was changed",
    )

    status_id = models.ForeignKey(
        BillStatus,
        null=True,
        blank=True,
        help_text="Status that was changed",
    )
