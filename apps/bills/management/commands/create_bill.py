from django.core.management.base import BaseCommand
from django.utils import timezone
from apps.advertisers.models import Advertiser, AdvertiserOfferGoal, AdvertiserOfferConversion
from apps.bills.models import Bill, BillStatus, BillEntry
from django.db.models import Sum, Count
from django.conf import settings
from apps.authorization.utils import print_start_time, print_end_time
import datetime
from var_dump import var_dump


class Command(BaseCommand):
    def handle(self, *args, **options):
        print_start_time()
        current_date = datetime.date.today()
        current_day = current_date.day

        if current_day == 1:
            advertisers = Advertiser.objects.filter(status='active')
        elif current_day == 15:
            advertisers = Advertiser.objects.filter(status='active', billing_period=2)
        else:
            print("Today is not a day to generating bills")
            return False

        for advertiser in advertisers:
            if advertiser.billing_period == 1 and current_day == 1:
                date_from = datetime.date(current_date.year, current_date.month - 1, 1)
            elif advertiser.billing_period == 2 and current_day == 1:
                date_from = datetime.date(current_date.year, current_date.month - 1, 15)
            elif advertiser.billing_period == 2 and current_day == 15:
                date_from = datetime.date(current_date.year, current_date.month, 1)
            else:
                print("Fatal error for advertiser %s. Billing_period is not defined - can not define date range for bill." % advertiser)
            date_to = current_date

            print("\r\nCurrent advertiser is %s.\r\nBilling_period: date_from = %s, date_to = %s" % (advertiser, date_from, date_to))

            conversions = AdvertiserOfferConversion.objects.filter(advertiser_id=advertiser.pk,
                                                                   date_time__range=(date_from, date_to))
            amount_without_vat_revenue = conversions.aggregate(Sum('revenue'))
            amount_without_vat = amount_without_vat_revenue['revenue__sum']

            vat_amount = amount_without_vat * settings.VAT_RATE if advertiser.vat_enabled and amount_without_vat else 0
            amount_with_vat = amount_without_vat * settings.AMOUNT_WITH_VAT_MULTIPLIER if advertiser.vat_enabled and amount_without_vat else amount_without_vat
            acc_status = BillStatus.objects.get(type='acc', ordering=0)
            stat_status = BillStatus.objects.get(type='stat', ordering=0)

            bill = Bill.objects.create(
                code='б/н',
                create_date=timezone.localdate(),
                create_time=timezone.localtime(),
                period_from=date_from,
                period_to=date_to,
                advertiser_id=advertiser,
                vat_enabled=advertiser.vat_enabled,
                amount_without_vat=amount_without_vat,
                vat_amount=vat_amount,
                amount_with_vat=amount_with_vat,
                acc_status=acc_status,
                stat_status=stat_status,
            )

            print("For advertiser %s was created a bill from %s with total amount %s " % (
            bill.advertiser_id, bill.create_date, bill.amount_with_vat))

            goals = AdvertiserOfferGoal.objects.filter(advertiser_id=advertiser, status='active').select_related(
                'offer_id')
            for goal in goals:
                unique_revenue_values = conversions.filter(goal_id=goal).values('revenue').distinct().order_by(
                    'revenue')

                # for every unique revenue amount generates independent entry
                if unique_revenue_values:
                    for value in unique_revenue_values:
                        leads_revenue = value['revenue']
                        leads = conversions.filter(goal_id=goal, revenue=leads_revenue)

                        leads_sum_count = leads.aggregate(Count('revenue'), Sum('revenue'))
                        leads_count = leads_sum_count['revenue__count']
                        leads_sum = leads_sum_count['revenue__sum']
                        vat_amount = leads_sum * settings.VAT_RATE if advertiser.vat_enabled and leads_sum else 0
                        amount_with_vat = leads_sum * settings.AMOUNT_WITH_VAT_MULTIPLIER if advertiser.vat_enabled and leads_sum else leads_sum

                        bill_entry = BillEntry.objects.create(
                            bill_id=bill,
                            offer_id=goal.offer_id,
                            goal_id=goal,
                            name="%s - %s - %s" % (goal.offer_id.unique_id, goal.offer_id.name, goal.name),
                            leads=leads_count,
                            cost=leads_revenue,
                            amount_without_vat=leads_sum,
                            vat_amount=vat_amount,
                            amount_with_vat=amount_with_vat,
                        )

                        if leads:
                            n = 0
                            limit = 100
                            add = 100
                            while n <= leads_count:
                                leads = leads[n:limit]
                                bill_entry.conversions.add(*leads)
                                limit += add
                                n += add

                        print(
                            "Bill entry: %s with total amount %s was created. Entry was related with %s conversions." % (
                            bill_entry.name, bill_entry.amount_with_vat, leads_count))
                else:
                    bill_entry = BillEntry.objects.create(
                        bill_id=bill,
                        offer_id=goal.offer_id,
                        goal_id=goal,
                        name="%s - %s - %s" % (goal.offer_id.unique_id, goal.offer_id.name, goal.name),
                        leads=0,
                        cost=goal.max_payout,
                        amount_without_vat=0,
                        vat_amount=0,
                        amount_with_vat=0,
                    )
                    print(
                        "Bill entry: %s with total amount %s was created." % (
                            bill_entry.name, bill_entry.amount_with_vat))
        print_end_time()
