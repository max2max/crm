# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-07 12:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('advertisers', '0022_auto_20171103_1632'),
        ('bills', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BillEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(blank=True, null=True)),
                ('leads', models.PositiveIntegerField(blank=True, null=True)),
                ('cost', models.PositiveIntegerField(blank=True, null=True)),
                ('amount', models.PositiveIntegerField(blank=True, null=True)),
                ('vat_amount', models.PositiveIntegerField(blank=True, null=True)),
                ('amount_with_vat', models.PositiveIntegerField(blank=True, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='bill',
            name='offers_id',
        ),
        migrations.AddField(
            model_name='billentry',
            name='bill_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bills.Bill'),
        ),
        migrations.AddField(
            model_name='billentry',
            name='offer_id',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='advertisers.AdvertiserOffer'),
        ),
    ]
