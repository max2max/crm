# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-17 10:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bills', '0020_billentry_conversions'),
    ]

    operations = [
        migrations.AlterField(
            model_name='billentry',
            name='conversions',
            field=models.ManyToManyField(blank=True, to='advertisers.AdvertiserOfferConversion'),
        ),
    ]
