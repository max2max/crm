# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-08 14:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bills', '0014_auto_20171108_1454'),
    ]

    operations = [
        migrations.AddField(
            model_name='billfiles',
            name='create_date',
            field=models.DateTimeField(blank=True, help_text='Date of fileupload', null=True),
        ),
    ]
