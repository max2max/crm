# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-07 11:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('advertisers', '0022_auto_20171103_1632'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bill',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.TextField(blank=True, help_text='Code of bill in 1C', null=True)),
                ('create_date', models.DateTimeField(blank=True, help_text='The date bill was created in', null=True)),
                ('period_from', models.DateTimeField(blank=True, help_text='Start of billing period', null=True)),
                ('period_to', models.DateTimeField(blank=True, help_text='End of billing period', null=True)),
                ('vat_enabled', models.BooleanField(default=False, help_text='Enabled VAT for this Bill')),
                ('advertiser_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='advertiser', to='advertisers.Advertiser')),
                ('offers_id', models.ManyToManyField(related_name='offers', to='advertisers.AdvertiserOffer')),
            ],
        ),
    ]
