# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-07 15:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bills', '0012_bill_acc_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='bill',
            name='stat_status',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='stat_status', to='bills.BillStatus'),
        ),
        migrations.AlterField(
            model_name='bill',
            name='acc_status',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='acc_status', to='bills.BillStatus'),
        ),
    ]
