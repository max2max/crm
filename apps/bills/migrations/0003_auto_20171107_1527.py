# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-07 12:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('bills', '0002_auto_20171107_1507'),
    ]

    operations = [
        migrations.CreateModel(
            name='BillStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(blank=True, help_text='Name of status')),
                ('ordering', models.PositiveIntegerField(default=555, help_text='Bill ordering for workflow')),
                ('action_forward_name', models.TextField(default='Подтвердить', help_text='Name of action for next state')),
                ('action_backward_name', models.TextField(default='Вернуть', help_text='Name of action for previous state')),
                ('type', models.CharField(choices=[('stat', 'statistics'), ('acc', 'accountant')], max_length=4)),
                ('change_permitted', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='auth.Group')),
            ],
        ),
        migrations.RemoveField(
            model_name='billentry',
            name='amount',
        ),
        migrations.AddField(
            model_name='bill',
            name='amount_with_vat',
            field=models.PositiveIntegerField(blank=True, help_text='Total bill amount included VAT - *key* amount', null=True),
        ),
        migrations.AddField(
            model_name='bill',
            name='amount_without_vat',
            field=models.PositiveIntegerField(blank=True, help_text='Total bill amount excluded VAT', null=True),
        ),
        migrations.AddField(
            model_name='bill',
            name='confirmed',
            field=models.BooleanField(default=False, help_text='Is bill confiremed by the manager'),
        ),
        migrations.AddField(
            model_name='bill',
            name='vat_amount',
            field=models.PositiveIntegerField(blank=True, help_text='Total bill VAT amount', null=True),
        ),
        migrations.AddField(
            model_name='billentry',
            name='amount_without_vat',
            field=models.PositiveIntegerField(blank=True, help_text='Total entry amount excluded VAT', null=True),
        ),
        migrations.AlterField(
            model_name='billentry',
            name='amount_with_vat',
            field=models.PositiveIntegerField(blank=True, help_text='Total entry amount', null=True),
        ),
        migrations.AlterField(
            model_name='billentry',
            name='cost',
            field=models.PositiveIntegerField(blank=True, help_text='Cost of the one lead, imported from HasOffers or edited/filled in manually by manager', null=True),
        ),
        migrations.AlterField(
            model_name='billentry',
            name='leads',
            field=models.PositiveIntegerField(blank=True, help_text='Amount of leads, imported from HasOffers or edited/filled in manually by manager', null=True),
        ),
        migrations.AlterField(
            model_name='billentry',
            name='name',
            field=models.TextField(blank=True, help_text='Name of bill entry: Offer name from HasOffers or custom name filled in by manager', null=True),
        ),
        migrations.AlterField(
            model_name='billentry',
            name='vat_amount',
            field=models.PositiveIntegerField(blank=True, help_text='Total entry VAT amount', null=True),
        ),
    ]
