# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-07 13:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bills', '0008_auto_20171107_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='billstatus',
            name='action_backward_name',
            field=models.TextField(blank=True, default='Вернуть', help_text='Name of action for previous state'),
        ),
        migrations.AlterField(
            model_name='billstatus',
            name='action_forward_name',
            field=models.TextField(blank=True, default='Подтвердить', help_text='Name of action for next state'),
        ),
    ]
