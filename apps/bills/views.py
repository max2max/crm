from django.http import HttpResponse, Http404, HttpRequest
from django.views import View
from django.template import loader, RequestContext
from .models import Bill, BillComments, BillFiles, BillStatus, BillEntry
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.mail import send_mail
from django.contrib.auth import get_user
from backend import settings
from var_dump import var_dump


class BillList(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = get_user(request)
        filter_for_query = dict()
        if user.is_superuser or user.groups.filter(name='accountant').exists() or user.groups.filter(name='statistics').exists():
            filter_for_query = dict()
        elif user.groups.filter(name='manager').exists():
            filter_for_query = {'advertiser_id__account_manager_id': user.pk}

        bills = Bill.objects.filter(**filter_for_query).order_by('-create_date', 'advertiser_id__unique_id').select_related('advertiser_id', 'acc_status', 'stat_status')
        bill_dates = Bill.objects.filter(**filter_for_query).values('create_date').distinct().order_by('-create_date')

        template = loader.get_template('bill/bill_list.html')
        context = {
            'bills': bills,
            'unique_bill_dates': bill_dates,
        }
        return HttpResponse(template.render(context, request))


class BillDetail(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        pk = kwargs['id']
        bill_query_set = Bill.objects.filter(pk=pk).select_related('advertiser_id', 'acc_status', 'stat_status')
        bill = bill_query_set[0] if bill_query_set else Http404
        bill_entry = BillEntry.objects.filter(bill_id=bill).select_related('offer_id')
        bill_comments_acc = BillComments.objects.filter(bill_id=bill, type='acc').order_by('-create_date')
        bill_comments_stat = BillComments.objects.filter(bill_id=bill, type='stat').order_by('-create_date')
        bill_files_acc = BillFiles.objects.filter(bill_id=bill, type='acc').order_by('-create_date')
        bill_files_stat = BillFiles.objects.filter(bill_id=bill, type='stat').order_by('-create_date')
        bill_workflow = BillStatus.objects.all().order_by('ordering')

        template = loader.get_template('bill/bill_detail.html')
        context = {
            'bill': bill,
            'bill_entry': bill_entry,
            'bill_comments_acc': bill_comments_acc,
            'bill_comments_stat': bill_comments_stat,
            'bill_files_acc': bill_files_acc,
            'bill_files_stat': bill_files_stat,
            'bill_workflow': bill_workflow,
        }
        return HttpResponse(template.render(context, request))


class BillIndex(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):

        user = get_user(request)
        if user.is_superuser:
            statuses = BillStatus.objects.filter(is_last=False)
        else:
            user_group = user.groups.first()
            statuses = BillStatus.objects.filter(change_permitted_id=user_group, is_last=False)

        if user.groups.filter(name='manager').exists():
            filter_for_query = {'advertiser_id__account_manager_id': user.pk}
        else:
            filter_for_query = dict()

        bills = list()
        for status in statuses:
            if status.type == 'acc':
                bill = Bill.objects.filter(acc_status=status, **filter_for_query).order_by('-create_date' , 'advertiser_id__unique_id').select_related('advertiser_id', 'acc_status', 'stat_status')
            else:
                bill = Bill.objects.filter(is_stats_upload_necessary=True, stat_status=status, **filter_for_query).order_by('-create_date', 'advertiser_id__unique_id').select_related('advertiser_id', 'acc_status', 'stat_status')
            bill.what_is_need_to_do = status.what_is_need_to_do
            bills.append(bill)

        context = {
            'bills': bills,
        }
        template = loader.get_template('bill/bill_index.html')
        return HttpResponse(template.render(context, request))


def send_role_email(bill, new_status, request):
    email = None
    if new_status and new_status.is_last is not True:
        if new_status.change_permitted.name == 'manager':
            email = bill.advertiser_id.account_manager_id.email
            if email is None:
                email = settings.MANAGER_EMAIL
        elif new_status.change_permitted.name == 'statistic':
            email = settings.STATISTIC_EMAIL
        elif new_status.change_permitted.name == 'accountant':
            email = settings.ACCOUNTANT_EMAIL
    if settings.DEBUG:
        email = settings.DEBUG_EMAIL
    subject = 'Изменен статус счета: "%s" рекламодателя: "%s" на сумму: %s' % (bill.code, bill.advertiser_id, bill.amount_with_vat)
    link = '%s/bill/%s' % (HttpRequest.get_host(request), bill.pk)
    message = 'Изменен статус счета "%s" рекламодателя "%s" на сумму "%s".\r\n' \
              'Новый статус счета "%s".\r\n' \
              'Треубется %s\r\n' \
              'Счет доступен по ссылке: %s' % (bill.code, bill.advertiser_id, bill.amount_with_vat, new_status.name, new_status.action_forward_name, link)
    sent_from = 'crm@leadgid.ru'
    send_mail(
        subject,
        message,
        sent_from,
        [email, ],
        fail_silently=True,
    )
    return email or None
