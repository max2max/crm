from django.contrib import admin
from .models import Bill, BillStatus, BillEntry, BillComments, BillFiles, BillStatusHistory


class AdvertiserStatus(admin.ModelAdmin):
    pass


class BillEntryInline(admin.StackedInline):
    model = BillEntry
    extra = 1


class BillFilesInline(admin.StackedInline):
    model = BillFiles
    extra = 1


class BillCommentsInline(admin.StackedInline):
    model = BillComments
    extra = 1


class BillStatusHistoryInline(admin.StackedInline):
    model = BillStatusHistory
    extra = 1


class BillAdmin(admin.ModelAdmin):
    inlines = [BillCommentsInline, BillFilesInline, BillStatusHistoryInline]
    list_display = ['pk', 'code', '__str__', 'advertiser_id', 'create_date', 'amount_with_vat', 'vat_enabled', 'confirmed']


admin.site.register(Bill, BillAdmin)


class BillStatusAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'status_forward_id', 'action_forward_name', 'status_backward_id', 'action_backward_name', 'ordering', 'is_last', 'change_permitted_id',]


admin.site.register(BillStatus, BillStatusAdmin)
