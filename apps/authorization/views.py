from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.contrib import auth
from django.shortcuts import redirect
from django.urls import reverse
from django.http import HttpResponse
from django.views import View
from django.template import loader
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
import requests
from var_dump import var_dump


class Login(View):
    template = loader.get_template('auth/login.html')

    def get(self, request):
        return HttpResponse(self.template.render(dict(), request))

    def post(self, request):
        username = request.POST['email']
        password = request.POST['password']

        try:
            user = User.objects.get(username=username)
        except:
            context = {'result': 'Пользователь с e-mail`ом %s не найден.' % username}
            return HttpResponse(self.template.render(context, request))

        authenticated_user = authenticate(username=username, password=password)
        if authenticated_user is not None:
            if authenticated_user.is_active:
                auth.login(request, user)
                return redirect(reverse('bill_index'))
        else:
            unique_id = user.profile.unique_id
            base_url = settings.BASE_URL
            model = 'Employee'
            method = 'checkPassword'
            check_password_url = "%s&Target=%s&Method=%s&id=%s&password=%s" % (base_url, model, method, unique_id, password)

            r = requests.get(check_password_url)
            r_json = r.json()
            response_data = r_json.get('response').get('data')

            if user.is_active and response_data is True:
                auth.login(request, user)
                return redirect(reverse('bill_index'))
            else:
                context = {'result': 'Пароль некорректный. Попробуйте ввести пароль заново.'}
            return HttpResponse(self.template.render(context, request))


class Logout(View):
    def get(self, request):
        logout(request)
        return redirect(reverse('login'))


class LoginAs(View):
    @method_decorator(login_required)
    def get(self, request):
        new_user_id = request.GET['id']
        try:
            new_user = User.objects.get(pk=new_user_id)
        except:
            return redirect(reverse('bill_index'))
        current_user = request.user
        current_user_id = current_user.pk
        if new_user.is_active and current_user.is_superuser:
            logout(request)
            auth.login(request, new_user)
            request.session['prev_user_id'] = current_user_id
            return redirect(reverse('bill_index'))


class LogoutAs(View):
    @method_decorator(login_required)
    def get(self, request):
        user_id = int(request.session['prev_user_id'])
        try:
            user = User.objects.get(pk=user_id)
        except:
            logout(request)
            return redirect(reverse('login'))

        logout(request)

        if user.is_superuser:
            auth.login(request, user)
            return redirect(reverse('bill_index'))
        else:
            return redirect(reverse('login'))
