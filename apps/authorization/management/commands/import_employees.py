from django.core.management.base import BaseCommand
import requests
from django.conf import settings
from django.contrib.auth.models import User
from apps.authorization.utils import print_start_time, print_end_time


class Command(BaseCommand):
    help = 'Import Employees'
    model = "Employee"
    method = "findAll"
    base_url = settings.BASE_URL
    get_all_employees_url = "%s&Target=%s&Method=%s&fields[]=id&fields[]=last_name&fields[]=first_name&fields[]=email" % (base_url, model, method)

    def handle(self, *args, **options):
        print_start_time()
        r = requests.get(self.get_all_employees_url)
        r_json = r.json()
        response_data = r_json.get('response').get('data')
        n, m = 0, 0
        for item in response_data:
            employee = response_data[item]['Employee']
            try:
                user = User.objects.create_user(
                    employee['email'],
                    employee['email'],
                    'leadgid'
                )
                user.first_name = employee['first_name']
                user.last_name = employee['last_name']
                user.profile.unique_id = employee['id']
                print("User with email %s created" % (employee['email']))
                n += 1
            except:
                user = User.objects.get(username=employee['email'])
                user.first_name = employee['first_name']
                user.last_name = employee['last_name']
                user.profile.unique_id = employee['id']
                user.save()
                m += 1
        print("%s users were created.\r\n%s users were updated" % (n, m))
        print_end_time()



