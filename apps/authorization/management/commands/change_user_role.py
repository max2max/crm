from django.core.management.base import BaseCommand
from django.contrib.auth.models import User, Group
from apps.authorization.utils import print_start_time, print_end_time
from apps.advertisers.models import Advertiser


class Command(BaseCommand):

    def handle(self, *args, **options):
        print_start_time()
        advertisers = Advertiser.objects.all().select_related('account_manager_id').values('account_manager_id').order_by('account_manager_id').distinct()
        group = Group.objects.get(name='manager')
        users_of_group = group.user_set.all()
        n = 0
        for advertiser in advertisers:
            if advertiser['account_manager_id'] is not None:
                user = User.objects.get(pk=advertiser['account_manager_id'])
                if user not in users_of_group:
                    group.user_set.add(user)
                    n += 1
        print("Changing users roles finished. %s users affected." % n)
        print_end_time()
