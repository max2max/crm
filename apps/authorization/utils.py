from django.contrib.auth.models import User, Group
import datetime


def superuser_processor(request):
    if request.user.is_superuser:
        groups = Group.objects.all()
        for group in groups:
            users = User.objects.filter(groups=group)
            group.users = users
        return {
            'groups': groups,
        }
    else:
        return dict()


def print_start_time():
    print("\r\nCommand started running at %s" % datetime.datetime.now())


def print_end_time():
    print("\r\nCommand stopped running at %s" % datetime.datetime.now())
