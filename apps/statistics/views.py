from apps.bills.models import Bill, BillStatus
from django.contrib.auth.models import Group
from django.http import HttpResponse
from django.views import View
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse
from django.shortcuts import redirect
from var_dump import var_dump


class StatisticsManager(View):
    @method_decorator(login_required)
    def get(self, request):
        if request.user.is_superuser:
            template = loader.get_template('statistics/statistics_manager.html')
            group = Group.objects.get(name='manager')
            managers = group.user_set.all().order_by('first_name')
            unique_dates = Bill.objects.all().values('create_date').distinct().order_by('-create_date')
            bill_acc_statuses = BillStatus.objects.filter(type='acc')
            bill_stat_statuses = BillStatus.objects.filter(type='stat')
            result = dict()
            for unique_date in unique_dates:
                date = unique_date['create_date'].strftime('%d.%m.%Y')
                result[date] = dict()
                for manager in managers:
                    manager_name = manager.get_full_name() or manager.email
                    result[date][manager_name] = {
                        'acc_status': list(),
                        'stat_status': list(),
                        'percent_completed': float(),
                    }

                    for bill_acc_status in bill_acc_statuses:
                        bill_acc_count = Bill.objects.filter(advertiser_id__account_manager_id=manager,
                                                             acc_status=bill_acc_status,
                                                             create_date=unique_date['create_date']).count()
                        result[date][manager_name]['acc_status'].append(bill_acc_count)

                    for bill_stat_status in bill_stat_statuses:
                        bill_stat_count = Bill.objects.filter(advertiser_id__account_manager_id=manager,
                                                              stat_status=bill_stat_status,
                                                              create_date=unique_date['create_date']).count()
                        result[date][manager_name]['stat_status'].append(bill_stat_count)
                    bills_all_count = Bill.objects.filter(advertiser_id__account_manager_id=manager,
                                                          create_date=unique_date['create_date']).count()
                    bills_completed = Bill.objects.filter(advertiser_id__account_manager_id=manager,
                                                          create_date=unique_date['create_date'],
                                                          stat_status__is_last=True,
                                                          acc_status__is_last=True).select_related('acc_status',
                                                                                                   'stat_status').count()
                    if bills_all_count:
                        result[date][manager_name]['percent_completed'] = round(bills_completed / bills_all_count * 100, 2)
            context = {
                'data': result,
            }
            return HttpResponse(template.render(context, request))
        else:
            return redirect(reverse('bill_index'))
