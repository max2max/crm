from rest_framework import routers

from .views import AdvertiserViewSet, BillViewSet, BillEntryViewSet, AdvertiserOfferConversionViewSet, BillStatusViewSet, BillFilesViewSet, BillCommentsViewSet, BillHistoryViewSet, BillChecklistViewSet, UserViewSet

router = routers.DefaultRouter()
router.register('advertiser', AdvertiserViewSet)
router.register('bill', BillViewSet)
router.register('bill_entry', BillEntryViewSet)
router.register('conversion', AdvertiserOfferConversionViewSet)
router.register('bill_status', BillStatusViewSet)
router.register('bill_files', BillFilesViewSet)
router.register('bill_comments', BillCommentsViewSet)
router.register('bill_history', BillHistoryViewSet)
router.register('bill_checklist', BillChecklistViewSet)
router.register('user', UserViewSet)

urlpatterns = router.urls
