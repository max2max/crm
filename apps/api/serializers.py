from rest_framework import serializers

from apps.advertisers.models import Advertiser, AdvertiserOfferConversion
from apps.bills.models import Bill, BillEntry, BillStatus, BillStatusHistory, BillFiles, BillComments
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'pk',
            'email',
            'get_full_name',
        )


class AdvertiserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Advertiser
        fields = (
            'pk',
            'company',
            'unique_id',
        )


class BillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bill
        fields = '__all__'


class BillEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = BillEntry
        fields = (
            'id',
            'name',
            'leads',
            'cost',
            'amount_without_vat',
            'vat_amount',
            'amount_with_vat',
            'conversions',
            'offer_id',
            'bill_id',
        )


class AdvertiserOfferConversionSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdvertiserOfferConversion
        fields = (
            'id',
            'unique_id',
            'date_time',
            'revenue',
            'created_date_time',
        )


class BillStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillStatus
        fields = '__all__'


class BillCommentsSerizalizer(serializers.ModelSerializer):
    class Meta:
        model = BillComments
        fields = '__all__'


class BillFilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillFiles
        fields = '__all__'


class BillHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = BillStatusHistory
        fields = '__all__'


class BillChecklistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bill
        fields = (
            'is_sum_added',
            'is_bill_sent',
            'is_stat_sent',
            'is_stat_uploaded',
            'is_stat_checked',
        )
