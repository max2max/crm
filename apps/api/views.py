from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from .serializers import AdvertiserSerializer, BillSerializer, BillEntrySerializer, AdvertiserOfferConversionSerializer, BillStatusSerializer, BillFilesSerializer, BillCommentsSerizalizer, BillHistorySerializer, BillChecklistSerializer, UserSerializer
from apps.advertisers.models import Advertiser, AdvertiserOfferConversion
from apps.bills.models import Bill, BillEntry, BillStatus, BillComments, BillFiles, BillStatusHistory
from apps.bills.views import send_role_email
from django.contrib.auth.models import User
from var_dump import var_dump

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class AdvertiserViewSet(viewsets.ModelViewSet):
    serializer_class = AdvertiserSerializer
    queryset = Advertiser.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class BillChecklistViewSet(viewsets.ModelViewSet):
    serializer_class = BillChecklistSerializer
    queryset = Bill.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class BillViewSet(viewsets.ModelViewSet):
    serializer_class = BillSerializer
    queryset = Bill.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def partial_update(self, request, pk=None):
        serializer = BillSerializer(data=request.data)
        if serializer.is_valid():
            bill = self.get_object()
            bill_queryset = Bill.objects.filter(pk=bill.pk).select_related('advertiser_id')
            try:
                if bill.acc_status.id != serializer.data['acc_status']:
                    new_status = BillStatus.objects.get(pk=serializer.data['acc_status'])
                    mail = send_role_email(bill, new_status, request)
            except:
                pass
            try:
                if bill.stat_status.id != serializer.data['stat_status']:
                    new_status = BillStatus.objects.get(pk=serializer.data['stat_status'])
                    mail = send_role_email(bill, new_status, request)
            except:
                pass
            bill_queryset.update(**serializer.validated_data)
            return Response("Updated successfully")
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BillEntryViewSet(viewsets.ModelViewSet):
    serializer_class = BillEntrySerializer
    queryset = BillEntry.objects.all().select_related('offer_id').order_by('name')
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_fields = ('bill_id', 'id')


class AdvertiserOfferConversionViewSet(viewsets.ModelViewSet):
    serializer_class = AdvertiserOfferConversionSerializer
    queryset = AdvertiserOfferConversion.objects.all().order_by('unique_id')
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_fields = ('id',)


class BillStatusViewSet(viewsets.ModelViewSet):
    serializer_class = BillStatusSerializer
    queryset = BillStatus.objects.all().order_by('ordering')
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_fields = ('type',)


class BillCommentsViewSet(viewsets.ModelViewSet):
    serializer_class = BillCommentsSerizalizer
    queryset = BillComments.objects.all().order_by('-pk')
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_fields = ('bill_id', 'type',)


class BillFilesViewSet(viewsets.ModelViewSet):
    serializer_class = BillFilesSerializer
    queryset = BillFiles.objects.all().order_by('-pk')
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_fields = ('bill_id', 'type',)


class BillHistoryViewSet(viewsets.ModelViewSet):
    serializer_class = BillHistorySerializer
    queryset = BillStatusHistory.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

