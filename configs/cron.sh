#!/bin/bash
cd /var/www/crm/
source /var/www/crm/.env/bin/activate
python manage.py import_employees
python manage.py import_advertisers
python manage.py change_user_role
python manage.py import_users
python manage.py import_offers
python manage.py import_goals
python manage.py import_conversions_wo_advertiser
python manage.py create_bill
python manage.py dumpdata >> dump.json
