"""
    backend URL Configuration
"""
from django.conf.urls import url, include
from django.contrib import admin
from apps.advertisers.views import AdvertiserList, AdvertiserDetail
from apps.bills.views import BillList, BillDetail, BillIndex
from apps.authorization.views import Login, Logout, LoginAs, LogoutAs
from apps.statistics.views import StatisticsManager
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^login_as/$', LoginAs.as_view(), name='login_as'),
    url(r'^logout/$', Logout.as_view(), name='logout'),
    url(r'^logout_as/$', LogoutAs.as_view(), name='logout_as'),

    url(r'^advertiser/$', AdvertiserList.as_view(), name='advertiser_list'),
    url(r'^advertiser/(?P<unique_id>\d{,6})$', AdvertiserDetail.as_view()),


    url(r'^$', BillIndex.as_view(), name='bill_index'),
    url(r'^bill/$', BillList.as_view(), name='bill_list'),
    url(r'^bill/(?P<id>\d{,6})$', BillDetail.as_view(), name='bill_detail'),

    url(r'^statistics/$', StatisticsManager.as_view(), name='statistics_manager'),

    url(r'^api/v1/', include('apps.api.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
