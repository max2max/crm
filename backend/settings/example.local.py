"""
    This file no effect on Django application
    
    Example to use:

    # Copy file with `local.py` name and edit it.
    cp example.local.py local.py
"""

from .settings import PROJECT_DIR


ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_DIR, 'db.sqlite3'),
    }
}

DEBUG = True
