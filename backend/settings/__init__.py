"""
    Accumulating all project settings
"""

from django.core.exceptions import ImproperlyConfigured

from .settings import *

#try:
#    from .local import *
#except:
#    raise ImproperlyConfigured('App must have local settings. See example.local.py')